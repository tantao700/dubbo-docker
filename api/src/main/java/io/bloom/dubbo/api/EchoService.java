package io.bloom.dubbo.api;

public interface EchoService {
    String echo(String echo);
}
