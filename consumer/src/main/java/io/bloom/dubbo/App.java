package io.bloom.dubbo;

import io.bloom.dubbo.api.EchoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Hello world!
 */
@SpringBootApplication
@ImportResource("classpath:dubbo-consumer.xml")
@RestController
public class App {

    private final static Logger log = LoggerFactory.getLogger(App.class);

    @Autowired
    private EchoService service;

    public static void main(String[] args) {
        SpringApplication.run(App.class);
    }

    @RequestMapping("/")
    @ResponseBody
    public Map<String, String> index(@RequestParam(value = "hi", defaultValue = "guest") String hi) {

        final String echo = service.echo(hi);
        log.info(">>>>:{}", echo);
        System.out.println("Hello");
        return new HashMap<String, String>() {
            {
                put("response", echo);
            }
        };
    }
}
