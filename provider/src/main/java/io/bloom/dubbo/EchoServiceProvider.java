package io.bloom.dubbo;

import io.bloom.dubbo.api.EchoService;

public class EchoServiceProvider implements EchoService {
    @Override
    public String echo(String echo) {
        return "Hi " + echo;
    }
}
