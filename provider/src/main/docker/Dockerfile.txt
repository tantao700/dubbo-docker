FROM java:8-alpine

VOLUME /tmp
ADD provider.jar /opt/app/
EXPOSE 8080
WORKDIR /opt/app/
CMD ["java", "-Djava.security.egd=file:/dev/./urandom", "-Xms512m", "-Xmx1g", "-jar", "provider.jar"]